<?php 
    /*
        Call Event Detail from GoPlay Web API
        Need to add cookie
    */
    $HOST_WEB = "https://r7lk8n0srbams5t7sl7q.goplay.co.id";
    $PATH_EVENT_DETAIL = "/api/v1/live/event/";
    $req = $_GET["s"];
    $url = "$HOST_WEB$PATH_EVENT_DETAIL$req";
    $c = curl_init($url); 
    curl_setopt($c, CURLOPT_COOKIE, 'gp_fgp=0'); 
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1); 
    $response = curl_exec($c);
    curl_close($c); 

    /*
        Decode the json response, and get guard
    */
    $eventDetail = json_decode($response, true);
    $guardUrl = $eventDetail["data"]["guard_url"];

    if($guardUrl == "") {//For demo, hardcoded fallback on error
        $guardUrl = "wss://g-vanguard.goplay.co.id/live-guard?token=eyJhbGciOiJQQkVTMi1IUzI1NitBMTI4S1ciLCJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwicDJjIjoxMDAxLCJwMnMiOiJVdjM4QnlHQ1pVOFdQMThQbW1JZCJ9.nu_WIxiSszChas2QQftssZ1rGh9RCxYfafgnDxlSF6Gossc-tWK-wA.jQnqAOxvZiDjSQDowzNqKA.qgDo0GUtE9Py0kX8DAXLSoqKOfvhqZV0HLX0omESwsrewAMDotd9QC2Q90_5I1oeKKThfMmUYVRQJJSjJtspI-8obsfJ9kYHqPdEjKzTeG7CjzkMov_32kD7IQAta6d3O5Psf0wiNa9yXAABjQ-6MBAPqsGx5oEGrUpgGQRL_TPgv0jMir116Jy1bxI0xMlAp95Zut9I-xQMpl52isyNZ7SG_206zUgfZ7mRPahZkb5lDaJyDrDfHVpaSIgberPSoBYvyw-Tv1Mf1w1dS0ygYKYSRHFCrPvA_vZBxtepaSorzpymEPOB_eFzW8ISB6Sz1QIrMjq4bUgzp3ItycRh1DB7sEHJLb8ASR8_WJjKVm2zcU6L1kbsDAVIVI_5tLepF-EKOikZgxtPw6FeF83gux3zbJ35UH1PUaK1NczmdupiDRDtIZt3JFkG6GZOoDEFNkkbnKuGXhKTJErSUzJsqh-vUqX2VKNUnTfsEN0_KvValmgcwABFJ5yE3qd06vWk5sSfLETpGZmuaGmq9bcFNxifK_cTUPA48Ckovmz1tWU.2TQE7vKonetptxRRU8cXuw";
    }
?>

<!DOCTYPE html>
<html>
<head> 
    <meta charset="utf-8">     
    <link rel="stylesheet" href="./style.css">
    <script src="./confetti.js"></script> <!-- Some libraries -->
    <script src="./chat-basic.js"></script>
</head>
<body>
    <!-- Chat Window -->
    <div>
        <div id="chat_box">
            <div id="chat_body" class="default"></div>
        </div>
    </div>
    <br>
    
    <!-- Performer can interact with the browser from OBS -->
    <div class="controll">
        <button class="button-start" onclick="onStartClicked(event)">Start Sound Effect</button>
    </div>

    <script>
        const guardUrl = "<?php echo $guardUrl; ?>";
        var active = false;
        var giftCounter = {} 

        function onStartClicked(e) {
            active = !active;
            if( !active ) {
                e.target.classList.remove("active");
                disconnect();
                UpdateGiftCounter();
                giftCounter = {}
            } else {
                e.target.classList.add("active");
                callbacks = setCallbacks(onChat, null, null, null, onMerchandise, null, onGift);
                connect(guardUrl, callbacks);
            }
        }
            
        const onGift = function(evt) {
            giftCounter[evt.frm] = evt.price + (giftCounter[evt.frm]?giftCounter[evt.frm]:0);
            if(giftCounter[evt.frm] >= 1000) {
                audio = new Audio('audio-applause.wav');
                audio.play();

                confetti.start();
                setTimeout(function() {
                    confetti.stop();
                }, 5000);
            } else if(giftCounter[evt.frm] >= 500) {
                audio = new Audio('audio-thanku.wav');
                audio.play();
            } else {
                audio = new Audio('audio-coins.mp3');
                audio.play();                
            }
            UpdateGiftCounter();
        }

        const onHeart = function(evt) { }
        const onMerchandise = function(evt) { }
        const onChat = function(evt) { }

        function getRandomColor() {
            let color = "#";
            for (let i = 0; i < 3; i++)
                color += ("0" + Math.floor(((1 + Math.random()) * Math.pow(16, 2)) / 2).toString(16)).slice(-2);
            return color;
        }
        
        function UpdateGiftCounter() {
            let chatBody = document.getElementById("chat_body");
            while (chatBody.firstChild) {
                chatBody.firstChild.remove();
            }
            if(!active) {
                return;
            }

            Object.keys(giftCounter).forEach(function(key) {
                var chat_obj = "<div class='chat_row'>" +
                    "<span style='color:" + getRandomColor() + ";' class='chat_from'>" + key + "</span>" + 
                    "<span class='chat_text'>: Rp." + giftCounter[key] + "</span></div>";
                var d = document.createElement("div");
                d.innerHTML = chat_obj;        
                chatBody.appendChild(d);  
            });
        }
        
    </script>
</body>
</html>

